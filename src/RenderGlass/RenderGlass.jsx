import React, { Component } from "react";
import { glassList } from "./glassList";

export default class RenderGlass extends Component {
  state = {
    detailGlass: glassList[0].url,
  };

  handleChangeGlass = (glass) => {
    this.setState({
      detailGlass: glass.url,
    });
  };

  renderGlass = () => {
    let glassArr = glassList.map((item) => {
      return (
        <div className="col-2">
          <div className="card">
            <img src={item.url} alt="" />
            <div className="card-body">
              <p>{item.name}</p>
              <button
                onClick={() => {
                  this.handleChangeGlass(item);
                }}
                className="btn btn-primary"
              >
                Click
              </button>
            </div>
          </div>
        </div>
      );
    });
    return glassArr;
  };
  render() {
    return (
      <div>
        <div style={{ position: "relative" }}>
          <img
            style={{ width: "250px", objectFit: "cover" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
        </div>

        <div style={{ position: "absolute", top: "72px", left: "560px" }}>
          <img style={{ width: "43%" }} src={this.state.detailGlass} alt="" />
        </div>

        <div className=" row">{this.renderGlass()}</div>
      </div>
    );
  }
}
